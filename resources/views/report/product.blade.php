<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')

@section('styles')

@endsection

@section('content')

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <div class="box-title">
                        Daftar Produk
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-gear"></i></span>
                        <select id="jenis" name="jenis" class="form-control" onchange="getData('0','0')">
                            <option value="0"> Semua</option>
                            <option value="1"> Produk</option>
                            <option value="2"> Jasa</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pull-right">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" class="form-control" name="daterange" value=""/>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">
                    <table id='table' class="table table-hover table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Produk</th>
                            <th>Total Penjualan</th>
                            <th>Total Penghasilan</th>
                        </tr>
                        </thead>
                        <tbody id="data">
                        @foreach($result as $row)
                            <tr>
                                <td>{{$row['no']}}</td>
                                <td>{{$row['nama']}}</td>
                                <td class="text-center">{{$row['total']}}</td>
                                <td class="text-right">{{$row['total_price']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script>

        function getData(sDate, eDate) {

            if (sDate == '0' && eDate == '0') {
                var str = $('input[name="daterange"]').val();

                sDate = str.substring(6, 10) + '-' + str.substring(0, 2) + '-' + str.substring(3, 5);
                eDate = str.substring(19, 23) + '-' + str.substring(13, 15) + '-' + str.substring(16, 18);
            }

            $.ajax({
                url: "produk-datatable",
                type: "POST",
                data: {
                    jenis: $('#jenis').val(),
                    startDate: sDate,
                    endDate: eDate
                },
                success: function (result) {
                    if (result.length > 0) {
                        $('#table').DataTable().destroy();

                        $("#data").children().remove();
                        $.each(result.slice(0, result.length), function (i, data) {
                            $("#data").append("<tr>" +
                                "<td>" + data.no + "</td>" +
                                "<td>" + data.nama + "</td>" +
                                "<td class='text-center'>" + data.total + "</td>" +
                                "<td class='text-right'>" + data.total_price + "</td>" +
                                "</tr>");
                        });

                        $('#table').DataTable({
                            "order": [] //Initial no order.
                        });
                    }
                },
                error: function (e) {
                    alert('Error occurred while request data');
                }
            });
        }

        $(function () {

            if ($('input[name="daterange"]').val() == '' || $('input[name="daterange"]').val() == null) {
                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();
                $('input[name="daterange"]').val(mm + '/01/' + yyyy + ' - ' + mm + '/' + dd + '/' + yyyy);
            }

            // $('#table').DataTable();
            $('#table').DataTable({
                "order": [] //Initial no order.
            });

            $('input[name="daterange"]').daterangepicker({
                opens: "right",
                ranges: {
                    'Hari Ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '1 Minggu Lalu': [moment().subtract(6, 'days'), moment()],
                    '1 Bulan Lalu': [moment().subtract(29, 'days'), moment()],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')]
                },
                alwaysShowCalendars: true,
                "locale": {
                    "separator": " - ",
                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
                    "monthNames": ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                    "firstDay": 1
                }
            });

            $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
                ev.preventDefault();
                getData(picker.startDate.format('YYYY-MM-DD'), picker.endDate.format('YYYY-MM-DD'));

                // table.ajax.reload(); //just reload table
                /*$('#table').DataTable().destroy();
                $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    pageLength: 25,
                    lengthMenu: [
                        [25, 50, 100, 200],
                        [25, 50, 100, 200]
                    ],
                    ajax: {
                        "url": "produk-datatable",
                        "type": "POST",
                        "data": function(data) {
                            data.startDate = picker.startDate.format('YYYY-MM-DD');
                            data.endDate = picker.endDate.format('YYYY-MM-DD');
                        }
                    }
                });*/
            });

        });
    </script>
@endsection