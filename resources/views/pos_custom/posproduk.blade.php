<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  {{-- <div class="content"> --}}
<div class="row">
  <form action="{{ CRUDBooster::mainpath('add-save') }}" method="POST">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-body">
          <div class="row">
            <div class="form-group col-sm-3">
              <label for="kode">Kode</label>
              <input type="text" name="kode" class="form-control" value="{{ $kode }}" readonly>
            </div>
            <div class="form-group col-sm-3">
              <label for="tanggal">Tanggal</label>
              <input type="date" name="tanggal" class="form-control" value="{{ date('Y-m-d') }}">
            </div>
            <div class="form-group col-sm-4">
              <label for="nama_pelanggan">Pelanggan</label>
              <select name="nama_pelanggan" class="form-control">
                <option value="" selected>Walk in Order</option>
                @foreach ($pelanggan as $p)
                  <option value="{{ $p->nama }}">{{ $p->nama }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-sm-2">
              <label for="total">Total</label>
              <input type="text" name="total" id="total_after" class="form-control" value="0" readonly>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <!-- Custom Tabs -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Pilih Barang</h3>
        </div>
        <div class="nav-tabs-custom">
          <?php $i = 1; ?>
          <ul class="nav nav-tabs">
            @foreach ($kategori as $kat)
            <li {{ $i == 1 ? 'class="active"' : ''}} ><a href="#tab_{{ $i }}" data-toggle="tab">{{ ucwords($kat->keterangan) }}</a></li>
            <?php $i++; ?>
            @endforeach
          </ul>
          <div class="tab-content">
            <?php $i = 1; ?>
            @foreach ($kategori as $kat)
            <div class="tab-pane {{ $i == 1 ? 'active' : ''}}" id="tab_{{ $i }}">
              <div class="row" style="height: 610px; overflow-y: scroll;">
                @foreach (${ str_replace(' ', '_', strtolower($kat->keterangan)) } as $p)
                <div class="col-md-3" style="max-height: 100%; margin-bottom: 10px">
                  <button type="button" data-name="{{ $p->keterangan }}" id="{{ $p->kode }}" type="button" value='{{ $p->id }}' class="btn btn-both btn-flat product" style="height: 150px; width:115%; white-space: normal; word-wrap: break-word;" {{ $p->stok == 0 ? 'disabled' : ''}}>
                  <span class="bg-img"><img src="{{ url('/') . '/' . $p->gambar }}" alt="{{ $p->keterangan }}" style="width: 50px; height: 50px;"></span>
                    <br><span>{{ $p->keterangan }}</span>
                    <br><span>{{ "Stok : " . $p->stok }}</span>
                  </button>
                </div>
                @endforeach
              </div>
              <?php $i++; ?>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Penjualan</h3>
        </div>
        <div class="box-body chat">
          <div class="row" style="height: 300px; overflow-y: scroll">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-md-12">
              <div class="row" style="border-bottom: 1px solid #eeeeee;margin-bottom: 15px;padding-bottom: 5px; max-width: 100%">
                <div class="col-md-5 text-center">Nama</div>
                <div class="col-md-2 text-center">Harga</div>
                <div class="col-md-1 text-center">Qty</div>
                <div class="col-md-2 text-center">Subtotal</div>
              </div>
              <div id="daftar-penjualan"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-body">
          <div class="form-horizontal">
            <div class="form-group">
              <label for="total" class="col-md-4 control-label">Subtotal</label>
              <label for="total" class="col-md-4 control-label" id="text-total">0</label>
              <input type="hidden" name="subtotal" id="total" class="form-control input-sm text-right" value="0" readonly>
            </div>
            <div class="form-group">
              <label for="diskon" class="col-md-4 control-label">Diskon (Tipe)</label>
              <div class="col-md-4">
                <input type="text" name="diskon_nominal" id="diskon" class="form-control" value="0">
              </div>
              <div class="col-md-4">
                <select name="diskon_tipe" id="tipe-diskon" class="form-control">
                  @foreach ($diskon as $d)
                  <option value="{{ $d->keterangan == "Nominal" ? 0 : 1 }}">{{ $d->keterangan }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="metode_pembayaran" class="col-md-4 control-label">Metode Pembayaran</label>
              <div class="col-md-8">
              @foreach ($mtd_byr as $mb)
              <div class="radio-inline">
                <label>
                <input type="radio" name="metode_pembayaran" value="{{ $mb->id }}" {{ $mb->id == 29 ? 'checked' : '' }}>
                {{ $mb->keterangan }}
                </label>
              </div>
              @endforeach
              </div>
            </div>
            <div class="form-group">
              <label for="merchant" class="col-md-4 control-label">Merchant (No. Kartu)</label>
              <div class="col-md-4">
                <select name="id_merchant" id="merchant" class="form-control" disabled>
                  <option selected>Pilih Merchant</option>
                  @foreach ($merchant as $d)
                  <option value="{{ $d->id }}">{{ $d->keterangan }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-4">
                <input type="text" name="nomor_kartu" class="form-control" id="nomor_kartu" disabled>
              </div>
            </div>
            <div class="form-group">
              <label for="diskon" class="col-md-4 control-label">Kode Trace</label>
              <div class="col-md-8">
                <input type="text" name="kode_trace" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label for="diskon" class="col-md-4 control-label">Bayar* (Kembalian)</label>
              <div class="col-md-4">
                <input type="text" name="bayar" id="bayar" class="form-control">
              </div>
              <div class="col-md-4">
                <input type="text" name="kembalian" id="kembalian" class="form-control" value="0" readonly>
              </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Save">
            <input type="button" class="btn btn-default" value="Cancel" onclick="history.back()">
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function() {
    $('select').select2();
    var i = 1;

    function subtotal(total, kembalian){
      var display = "0", display_kembalian = "0";
      if (total != 0){ display = total + ""; } else { display = "0"; }
      if (kembalian != 0){ display_kembalian = kembalian + ""; } else { display_kembalian = "0"; }
      $('#text-total').html(formatNumber(display));
      $('#total').val(display);
      $('#total_after').val(display);
      $('#kembalian').val(display_kembalian);
    }

    $(document).on('click', '.inc', function(){
      var button_id = $(this).attr("id");
      button_id = button_id.split("-")[1];
      var harga = $('#harga'+button_id+'').val();
      var qty = $('#qty'+button_id+'').val();
      var stok = parseInt($('#row'+button_id+'').attr('data-stock'));
      stok--;
      $('#row'+button_id+'').attr('data-stock', stok);
      $('#qty'+button_id+'').val(parseInt(qty) + 1);
      $('#textqty'+button_id+'').html(parseInt(qty) + 1);
      $('#textsub'+button_id+'').html(formatNumber(parseInt(harga) * (parseInt(qty) + 1)));
      $('#subtotal'+button_id+'').val(parseInt(harga) * (parseInt(qty) + 1));
      var total = parseInt($('#total').val()) + parseInt(harga);
      if ($('#bayar').val() != '' && parseInt($('#bayar').val()) >= total){
        var kembalian = parseInt($('#kembalian').val()) - parseInt(harga);
        subtotal(total, kembalian);
      }
      if (stok == 0){
        $(this).hide();
      }
    });

    $(document).on('click', '.dec', function(){
      var button_id = $(this).attr("id");
      button_id = button_id.split("-")[1];
      var harga = $('#harga'+button_id+'').val();
      var qty = $('#qty'+button_id+'').val();
      var stok = parseInt($('#row'+button_id+'').attr('data-stock'));
      if (stok == 0){
        $('.inc').show();
      }
      stok++;
      $('#row'+button_id+'').attr('data-stock', stok);
      $('#qty'+button_id+'').val(parseInt(qty) - 1);
      $('#textqty'+button_id+'').html(parseInt(qty) - 1);
      $('#textsub'+button_id+'').html(formatNumber(parseInt(harga) * (parseInt(qty) - 1)));
      $('#subtotal'+button_id+'').val(parseInt(harga) * (parseInt(qty) - 1));
      if (parseInt(qty) - 1 == 0){
        var btn_id = $('#row'+button_id+'').attr("data-id");
        document.getElementById(btn_id).disabled = false;
        $('#row'+button_id+'').remove();
      }
      var total = parseInt($('#total').val()) - parseInt(harga);
      if ($('#bayar').val() != '' && parseInt($('#bayar').val()) >= total){
        var kembalian = parseInt($('#kembalian').val()) + parseInt(harga);
        subtotal(total, kembalian);
      }
    });

    $(document).on('click', '.remove', function(){  
      var button_id = $(this).attr("id");
      var btn_id = $('#row'+button_id+'').attr("data-id");
      document.getElementById(btn_id).disabled = false;
      var total = parseInt($('#total').val()) - parseInt($('#subtotal'+button_id+'').val());
      if ($('#bayar').val() != '' && parseInt($('#bayar').val()) >= total){
        var kembalian = parseInt($('#kembalian').val()) + parseInt($('#subtotal'+button_id+'').val());
        subtotal(total, kembalian);
      }
      $('#row'+button_id+'').remove();
    });
    
    $('.product').on('click', function(e){
      var product_id = $(this).val();
      var button_id = $(this).attr('id');
      var qty = 1;
      document.getElementById(button_id).disabled = true;
      $.get("{{ url('barang') }}/" + product_id, function(data, status){
        keterangan = data.keterangan;
        harga = data.harga;
        stok = data.stok;

        $("#daftar-penjualan").append('<div class="row" data-id="'+data.kode+'" data-stock="'+(stok - 1)+'" id="row'+i+'" style="padding-bottom: 5px; max-width: 100%">'+
          '<div class="col-md-5">'+ keterangan +'<input type="hidden" readonly class="namaproduk" name="detailpenjualan-nama[]" id="nama'+i+'" value="'+ keterangan +'">'+
          '<input type="hidden" readonly name="detailpenjualan-id_produk[]" id="id_produk'+i+'" value="'+product_id+'"></div>'+
          '<div class="col-md-2 text-center">'+formatNumber(harga)+'<input type="hidden" readonly name="detailpenjualan-harga[]" id="harga'+i+'" value="'+harga+'"></div>'+
          '<div class="col-md-1 text-center"><span id="textqty'+i+'">'+qty+'</span><input type="hidden" readonly class="form-control input-sm text-center" name="detailpenjualan-quantity[]" id="qty'+i+'" value="'+qty+'"></div>'+
          '<div class="col-md-2 text-center"><span id="textsub'+i+'">'+formatNumber(harga * qty)+'</span><input type="hidden" readonly name="detailpenjualan-subtotal[]" class="subtotal" id="subtotal'+i+'" value="'+(harga * qty)+'"></div>'+
          '<div class="col-md-2"><button type="button" class="btn btn-xs btn-success inc" id="tambah-'+i+'"><i class="fa fa-sm fa-plus"></i></button>'+
          '&nbsp;<button type="button" class="btn btn-xs btn-warning dec" id="kurang-'+i+'"><i class="fa fa-sm fa-minus"></i></button>'+
          '&nbsp;<button class="btn btn-xs btn-danger remove" id="'+i+'"><i class="fa fa-sm fa-trash"></i></button></div>'+
        '</div>');
        i++;
        var total = 0;
        var bayar = 0;
        if ($('#bayar').val() == ''){
          bayar = 0;
        } else {
          if ($('#tipe-diskon option:selected').val() != "29"){
            bayar = parseInt($('#total_after').val());
            $('#bayar').val(bayar);
          } else {
            bayar = parseInt($('#bayar').val());
          }
        }
        $(".subtotal").each(function() { total += parseInt($(this).val()); });
        $('#total').val(total);
        $('#text-total').html(formatNumber(total));
        $('#total_after').val(total);
        if (bayar > 0){
          var kembalian = bayar - total;
          $('#kembalian').val(kembalian);
        }
        e.preventDefault();
      });
    });

    function bayar(harga_after){
      $('#total_after').val(harga_after);
      var bayar = 0;
      if ($('#bayar').val() == ''){
        bayar = 0;
      } else {
        if ($('#tipe-diskon option:selected').val() != "29"){
          bayar = parseInt($('#total_after').val());
          $('#bayar').val(bayar);
        } else {
          bayar = parseInt($('#bayar').val());
        }
      }
      var kembalian = bayar - harga_after;
      $('#kembalian').val(kembalian);
    }

    $('#diskon').keyup(function(e){
      if (e.target.value == ""){
        $('#diskon').val("0");
      }
      var tipe_diskon = $('#tipe-diskon option:selected').html();
      var nominal = e.target.value;
      var harga = $('#total').val();
      var harga_after = harga;
      if (tipe_diskon == 'Nominal'){
        harga_after = harga - nominal;
      } else if (tipe_diskon == 'Persen'){
        harga_after = harga - (harga * nominal / 100);
      }
      bayar(harga_after);
    });

    $('#tipe-diskon').change(function(){
      var nominal = $('#diskon').val();
      var harga = $('#total').val();
      var harga_after = harga;
      if ($(this).val() == '35'){
        harga_after = harga - nominal;
      } else if ($(this).val() == '36'){
        harga_after = harga - (harga * nominal / 100);
      }  
      bayar(harga_after);
    });

    $('input:radio[name="metode_pembayaran"]').change(function() {
      if ($(this).val() == "29") {
        $('#merchant').attr('disabled', 'disabled');
        $('#nomor_kartu').attr('disabled', 'disabled');
        $('#bayar').removeAttr('readonly');
      } else {
        var bayar = $('#total_after').val();
        $('#nomor_kartu').removeAttr('disabled');
        $('#bayar').attr('readonly', 'readonly');
        $('#bayar').val(bayar);
        $('#kembalian').val(parseInt($("#bayar").val()) - parseInt(bayar));
        if ($(this).val() == "30") {
          $('#merchant').removeAttr('disabled');
        } else if ($(this).val() == "52") {
          $('#merchant').attr('disabled', 'disabled');
        }
      }
    });
    $('#bayar').keyup(function(e){ 
      var kembalian;
      if (e.target.value == '') {
        e.target.value = "0";
      } else {
        if (parseInt(e.target.value) < parseInt($('#total_after').val())){
          kembalian = 0;
        } else {
          kembalian = parseInt(e.target.value) - parseInt($('#total_after').val());
        }
        $('#kembalian').val(kembalian);
      }
    });
    $('input:text').keypress(function (event) {
      if (event.keyCode === 10 || event.keyCode === 13) {
        event.preventDefault();
      }
    });
    function formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
  });
</script>
@endsection