<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  {{-- <div class="content"> --}}
<div class="row">
  <form action="{{ CRUDBooster::mainpath('edit-save/' . $row->id) }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-body">
          <div class="form-group col-sm-3">
            <label for="kode">Kode</label>
            <input type="text" name="kode" class="form-control" value="{{ $row->kode }}" readonly>
          </div>
          <div class="form-group col-sm-3">
            <label for="tanggal">Tanggal</label>
            <input type="date" name="tanggal" class="form-control" value="{{ explode(' ', $row->tanggal)[0] }}">
            <input type="hidden" name="tanggal_masuk" value="{{ $row->tanggal }}">
          </div>
          <div class="form-group col-sm-4">
            <label for="nama_pelanggan">Pelanggan</label>
            <select name="id_pelanggan" class="form-control">
              <option value="" selected>Walk in Order</option>
              @foreach ($pelanggan as $p)
                <option value="{{ $p->id }}" {{ $p->id == $row->id_pelanggan ? 'selected' : '' }}>{{ $p->nama }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-sm-2">
            <label for="total">Total</label>
            <input type="text" name="total" id="total_after" class="form-control" value="{{ $row->total }}" readonly>
          </div>
          {{-- <div class="form-group col-sm-3">
            <label for="id_kendaraan">Kendaraan</label>
            <select name="id_kendaraan" class="form-control" id="kendaraan">
              @foreach ($kendaraan as $p)
                <option value="{{ $p->id }}" {{ $row->id_kendaraan == $p->id ? 'selected' : '' }}>{{ $p->merek . ' ' . $p->model }}</option>
              @endforeach
            </select>
          </div> --}}
          <div class="form-group col-sm-3">
            <label for="total">Nomor Polisi</label>
            <input type="text" name="nomor_polisi" class="form-control" value="{{ $row->nomor_polisi }}">
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Pilih Jasa</h3>
        </div>
        <div class="box-body">
          <div class="row" style="height: 676px; overflow-y: scroll">
            @foreach ($jasa as $p)
            <div class="col-md-3" style="max-height: 100%; margin-bottom: 10px">
              <button type="button" data-name="{{ $p->keterangan }}" id="{{ $p->kode }}" type="button" value='{{ $p->id }}' class="btn btn-both btn-flat product" style="height: 125px; width:115%; white-space: normal; word-wrap: break-word;" {{ in_array($p->keterangan, $selected_product) ? 'disabled' : '' }}>
              <span class="bg-img"><img src="{{ url('/') . '/' . $p->gambar }}" alt="{{ $p->keterangan }}" style="width: 50px; height: 50px;"></span>
                <br><span>{{ $p->keterangan }}</span>
              </button>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Penjualan Jasa</h3>
        </div>
        <div class="box-body">
          <div class="row" style="border-bottom: 1px solid #eeeeee;margin-bottom: 15px;padding-bottom: 5px; max-width: 100%">
            <div class="col-md-4 text-center">Nama</div>
            <div class="col-md-2 text-center">Harga</div>
            <div class="col-md-2 text-center">Durasi (menit)</div>
            <div class="col-md-2 text-center">Subtotal</div>
          </div>
          <div id="daftar-penjualan" style="height: 238px; overflow-x: visible; overflow-y: scroll">
            <?php $i = 1; ?>
            @foreach ($detail as $d)
            <div class="row" data-id="{{ $d->kode }}" id="row{{ $i }}" style="padding-bottom: 5px; max-width: 100%">
              <div class="col-md-4">{{ $d->nama_jasa }}<input type="hidden" readonly name="detailjasa-nama[]" id="nama{{ $i }}" value="{{ $d->nama_jasa }}">
              <input type="hidden" readonly name="detailjasa-id_jasa[]" id="id_produk{{ $i }}" value="{{ $d->id_jasa }}"></div>
              <div class="col-md-2 text-center">{{ $d->harga }}<input type="hidden" readonly name="detailjasa-harga[]" id="harga{{ $i }}" value="{{ $d->harga }}"></div>
              <div class="col-md-2 text-center">{{ $d->durasi }}<input type="hidden" readonly name="detailjasa-durasi[]" id="durasi{{ $i }}" value="{{ $d->durasi }}"></div>
              <div class="col-md-2 text-center"><span id="textsub{{ $i }}">{{ $d->subtotal }}</span><input type="hidden" readonly name="detailjasa-subtotal[]" class="subtotal" id="subtotal{{ $i }}" value="{{ $d->harga }}"></div>
              <div class="col-md-1"><button class="btn btn-xs btn-danger remove" id="{{ $i }}"><i class="fa fa-sm fa-trash"></i></button></div>
            </div>
            <?php $i++; ?>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-body">
          <div class="form-horizontal col-md-12">
            <div class="form-group">
              <label for="total" class="col-md-4 control-label text-right">Subtotal</label>
              <label for="total" class="col-md-4 control-label text-right" id="text-total">{{ $row->subtotal }}</label>
              <input type="hidden" name="subtotal" id="total" class="form-control input-sm text-right" value="{{ $row->subtotal }}" readonly>
            </div>
            <div class="form-group">
              <label for="diskon" class="col-md-4 control-label">Diskon (Tipe)</label>
              <div class="col-md-4">
                <input type="text" name="diskon_nominal" id="diskon" class="form-control" value="{{ $row->diskon_nominal }}">
              </div>
              <div class="col-md-4">
                <select name="diskon_tipe" id="tipe-diskon" class="form-control">
                  @foreach ($diskon as $d)
                  <option value="{{ $d->id }}" {{ $d->id == $row->diskon_tipe ? 'checked' : '' }}>{{ $d->keterangan }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="metode_pembayaran" class="col-md-4 control-label">Metode Pembayaran</label>
              <div class="col-md-8">
              @foreach ($mtd_byr as $mb)
              <div class="radio-inline">
                <label>
                <input type="radio" name="metode_pembayaran" value="{{ $mb->id }}" {{ $mb->id == $row->metode_pembayaran ? 'checked' : '' }}>
                {{ $mb->keterangan }}
                </label>
              </div>
              @endforeach
              </div>
            </div>
            <div class="form-group">
              <label for="merchant" class="col-md-4 control-label">Merchant (No. Kartu)</label>
              <div class="col-md-4">
                <select name="id_merchant" id="merchant" class="form-control" {{ $row->metode_pembayaran == 29 || $row->metode_pembayaran == '' || $row->metode_pembayaran == 52 ? 'disabled' : '' }}>
                  <option selected>Pilih Merchant</option>
                  @foreach ($merchant as $d)
                  <option value="{{ $d->id }}" {{ $row->id_merchant == $d->id ? 'selected' : '' }}>{{ $d->keterangan }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-4">
                <input type="text" name="nomor_kartu" class="form-control" id="nomor_kartu" value="{{ isset($row->nomor_kartu) ? $row->nomor_kartu : '' }}" {{ $row->metode_pembayaran == 29 || $row->metode_pembayaran == '' ? 'disabled' : '' }}>
              </div>
            </div>
            <div class="form-group">
              <label for="diskon" class="col-md-4 control-label">Kode Trace</label>
              <div class="col-md-8">
                <input type="text" name="kode_trace" class="form-control" value="{{ isset($row->kode_trace) ? $row->kode_trace : '' }}">
              </div>
            </div>
            <div class="form-group">
              <label for="diskon" class="col-md-4 control-label">Bayar* (Kembalian)</label>
              <div class="col-md-4">
                <input type="text" name="bayar" id="bayar" class="form-control" value="{{ $row->bayar }}">
              </div>
              <div class="col-md-4">
                <input type="text" name="kembalian" id="kembalian" class="form-control" value="{{ $row->kembalian }}" readonly>
              </div>
            </div>
          </div>
          <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-primary" value="Save">
            <input type="button" class="btn btn-default" value="Cancel" onclick="history.back()">
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function() {
    $('select').select2();
    var i = {{ $i }};

    function subtotal(total, kembalian){
      var display = "0", display_kembalian = "0";
      if (total != 0){ display = total + ""; } else { display = "0"; }
      if (kembalian != 0){ display_kembalian = kembalian + ""; } else { display_kembalian = "0"; }
      $('#text-total').html(formatNumber(display));
      $('#total').val(display);
      $('#total_after').val(display);
      $('#kembalian').val(display_kembalian);
    }

    $(document).on('click', '.remove', function(){  
      var button_id = $(this).attr("id");
      var btn_id = $('#row'+button_id+'').attr("data-id");
      document.getElementById(btn_id).disabled = false;
      var total = parseInt($('#total').val()) - parseInt($('#subtotal'+button_id+'').val());
      var kembalian = parseInt($('#kembalian').val()) + parseInt($('#subtotal'+button_id+'').val());
      subtotal(total, kembalian);
      $('#row'+button_id+'').remove();
    });
    
    $('.product').on('click', function(e){
      var product_id = $(this).val();
      var button_id = $(this).attr('id');
      var id_kendaraan = $('#kendaraan').val();
      document.getElementById(button_id).disabled = true;
      $.get("{{ url('jasa') }}/" + product_id, function(data, status){
        keterangan = data.keterangan;
        harga = data.harga;
        durasi = data.durasi;

        $("#daftar-penjualan").append('<div class="row" data-id="'+data.kode+'" id="row'+i+'" style="padding-bottom: 5px; max-width: 100%">'+
          '<div class="col-md-4">'+ keterangan +'<input type="hidden" readonly name="detailjasa-nama[]" id="nama'+i+'" value="'+ keterangan +'">'+
          '<input type="hidden" readonly name="detailjasa-id_jasa[]" id="id_produk'+i+'" value="'+product_id+'"></div>'+
          '<div class="col-md-2 text-center">'+formatNumber(harga)+'<input type="hidden" readonly name="detailjasa-harga[]" id="harga'+i+'" value="'+harga+'"></div>'+
          '<div class="col-md-2 text-center">'+durasi+'<input type="hidden" readonly name="detailjasa-durasi[]" id="durasi'+i+'" value="'+durasi+'"></div>'+
          '<div class="col-md-2 text-center"><span id="textsub'+i+'">'+formatNumber(harga)+'</span><input type="hidden" readonly name="detailjasa-subtotal[]" class="subtotal" id="subtotal'+i+'" value="'+harga+'"></div>'+
          '<div class="col-md-1"><button class="btn btn-xs btn-danger remove" id="'+i+'"><i class="fa fa-sm fa-trash"></i></button></div>'+
        '</div>');
        i++;

        var total = 0;
        var bayar = 0;
        if ($('#bayar').val() == ''){
          bayar = 0;
        } else {
          bayar = parseInt($('#bayar').val());
        }
        $(".subtotal").each(function() { total += parseInt($(this).val()); });
        $('#total').val(total);
        $('#text-total').html(formatNumber(total));
        $('#total_after').val(total);
        if (bayar > 0){
          var kembalian = bayar - total;
          $('#kembalian').val(kembalian);
        }
        $('#sub').val('');
        e.preventDefault();
      });
    });
    function bayar(harga_after){
      $('#total_after').val(harga_after);
      var bayar = 0;
      if ($('#bayar').val() == ''){
        bayar = 0;
      } else {
        if ($('#tipe-diskon option:selected').val() != "29"){
          bayar = parseInt($('#total_after').val());
          $('#bayar').val(bayar);
        } else {
          bayar = parseInt($('#bayar').val());
        }
      }
      var kembalian = bayar - harga_after;
      $('#kembalian').val(kembalian);
    }

    $('#diskon').keyup(function(e){
      if (e.target.value == ""){
        $('#diskon').val("0");
      }
      var tipe_diskon = $('#tipe-diskon option:selected').html();
      var nominal = e.target.value;
      var harga = $('#total').val();
      var harga_after = harga;
      if (tipe_diskon == 'Nominal'){
        harga_after = harga - nominal;
      } else if (tipe_diskon == 'Persen'){
        harga_after = harga - (harga * nominal / 100);
      }
      bayar(harga_after);
    });

    $('#tipe-diskon').change(function(){
      var nominal = $('#diskon').val();
      var harga = $('#total').val();
      var harga_after = harga;
      if ($(this).val() == '35'){
        harga_after = harga - nominal;
      } else if ($(this).val() == '36'){
        harga_after = harga - (harga * nominal / 100);
      }  
      bayar(harga_after);
    });

    $('input:radio[name="metode_pembayaran"]').change(function() {
      if ($(this).val() == "29") {
        $('#merchant').attr('disabled', 'disabled');
        $('#nomor_kartu').attr('disabled', 'disabled');
        $('#bayar').removeAttr('readonly');
      } else {
        var bayar = $('#total_after').val();
        $('#nomor_kartu').removeAttr('disabled');
        $('#bayar').attr('readonly', 'readonly');
        $('#bayar').val(bayar);
        $('#kembalian').val(parseInt($("#bayar").val()) - parseInt(bayar));
        if ($(this).val() == "30") {
          $('#merchant').removeAttr('disabled');
        } else if ($(this).val() == "52") {
          $('#merchant').attr('disabled', 'disabled');
        }
      }
    });
    $('#bayar').keyup(function(e){ 
      if (e.target.value == '') {
        e.target.value = "0";
      } else {
        var kembalian = parseInt(e.target.value) - parseInt($('#total_after').val());
        $('#kembalian').val(kembalian);
      }
    });
    $('input:text').keypress(function (event) {
      if (event.keyCode === 10 || event.keyCode === 13) {
        event.preventDefault();
      }
    });
    function formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
  });
</script>
@endsection