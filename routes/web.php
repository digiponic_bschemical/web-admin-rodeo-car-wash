<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/print', function () {
    $data = session()->get('message');        
    return view('print_barcode', $data);
});

Route::get('barang/{id}', 'ProdukController@getProduk');
Route::get('jasa/{id}', 'ProdukController@getJasa');

Route::group(['prefix' => 'admin'], function () {
  Route::post('vendor-datatable', 'AdminLaporanVendorJasa@getData');
  Route::post('produk-datatable', 'AdminLaporanProduk@getData');
});

Route::group(['prefix' => 'api'], function () {
    
    Route::group(['prefix' => 'alamat'], function(){
      Route::get('/', 'API\AlamatController@alamatList');
      Route::get('/{id}', 'API\AlamatController@get');
      Route::get('/hapus/{id}', 'API\AlamatController@hapus');
      Route::post('/tambah', 'API\AlamatController@insert');
      Route::post('/edit', 'API\AlamatController@update');
    });

    Route::group(['prefix' => 'select'], function(){
      Route::get('/prov', 'API\SelectController@provinsi');
      Route::get('/kota', 'API\SelectController@kota');
      Route::get('/kec', 'API\SelectController@kecamatan');
    });

    Route::group(['prefix' => 'homeservice'], function(){
      Route::post('/tambah', 'API\HomeServiceController@insert');
    });

    Route::group(['prefix' => 'produk'], function () {
        Route::get('/', 'ApiProdukController@data');
        Route::get('/search', 'ApiProdukController@search');
    });

    Route::group(['prefix' => 'bahan'], function () {
        Route::get('/', 'ApiBahanController@data');
        Route::get('/single', 'ApiBahanController@single');
        Route::get('/search', 'ApiBahanController@search');
    });

    Route::group(['prefix' => 'general'], function () {
        Route::get('/', 'ApiGeneralController@data');
    });

    Route::group(['prefix' => 'pelanggan'], function () {
        Route::post('/', 'ApiPelangganController@store');
        Route::post('/kendaraan', 'ApiPelangganController@storeKendaraan');
        Route::post('/alamat', 'ApiPelangganController@storeAlamat');

        Route::get('/', 'ApiPelangganController@data');
        Route::get('/kendaraan', 'ApiPelangganController@kendaraan');
        Route::get('/alamat', 'ApiPelangganController@alamat');
    });

    Route::group(['prefix' => 'pos'], function () {
        Route::post('/', 'ApiPOSController@store');
        Route::post('/kendaraan', 'ApiPOSController@storeKendaraan');
        Route::post('/alamat', 'ApiPOSController@storeAlamat');

        Route::get('/', 'ApiPOSController@data');
        Route::get('/kendaraan', 'ApiPOSController@kendaraan');
        Route::get('/alamat', 'ApiPOSController@alamat');
    });

    Route::group(['prefix' => 'posjasa'], function () {
        Route::post('/', 'ApiPOSJasaController@store');
        Route::post('/kendaraan', 'ApiPOSJasaController@storeKendaraan');
        Route::post('/alamat', 'ApiPOSJasaController@storeAlamat');

        Route::get('/', 'ApiPOSJasaController@data');
        Route::get('/kendaraan', 'ApiPOSJasaController@kendaraan');
        Route::get('/alamat', 'ApiPOSJasaController@alamat');
    });

    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'API\UserController@login');
        Route::post('daftar', 'API\UserController@register');
        Route::post('edit', 'API\UserController@edit');
        Route::get('detail/{id}', 'API\UserController@detail');
        Route::get('logout', 'API\UserController@logout');
    });

    Route::group(['prefix' => 'kendaraan'], function () {
        Route::get('daftar/{merk}', 'API\KendaraanController@allByMerk');
        Route::get('merk', 'API\KendaraanController@merkList');
        Route::get('jenis', 'API\KendaraanController@vehicleType');
        Route::get('pelanggan/{idPelanggan}', 'API\KendaraanController@kendaraanPelanggan');
        Route::get('detail/{idKendaraan}', 'API\KendaraanController@detail');
        Route::post('tambah', 'API\KendaraanController@tambah');
        Route::post('edit', 'API\KendaraanController@edit');
        Route::delete('hapus/{id}', 'API\KendaraanController@hapus');
    });

    Route::group(['prefix' => 'jasa'], function () {
        Route::get('jenis', 'API\JasaController@jenisJasaList');
        Route::get('daftar/{id_jenis}', 'API\JasaController@jasaList');
        Route::get('harga', 'API\JasaController@hargaJasaList');
        Route::get('durasi', 'API\JasaController@durasiJasaList');
        Route::get('carwash', 'API\JasaController@allCarWash');
        Route::get('list/{id_kendaraan}/{jenis_jasa}', 'API\JasaController@byVehicle');
        Route::get('hargaperkendaraan', 'ApiJasaController@hargaPerKendaraan');
    });

    Route::group(['prefix' => 'reservasi'], function () {
        Route::get('cabang', 'API\ReservasiController@cabangList');
        Route::get('slots/{cabang}/{tgl}', 'API\ReservasiController@slotList');
        Route::post('tambah', 'API\ReservasiController@daftar');
        Route::get('pelanggan/{id}/{status}', 'API\ReservasiController@reservasiPelanggan');
        Route::get('detail/{kodebooking}', 'API\ReservasiController@detail');
        Route::post('ots', 'API\ReservasiController@ots');
        Route::get('all', 'API\ReservasiController@all');
    });

    Route::group(['prefix' => 'produk'], function () {
        Route::get('/', 'API\ProdukController@all');
        Route::get('/kategori', 'API\ProdukController@category');
        Route::get('/by-kategori/{kategori}', 'API\ProdukController@byCategory');
    });
	
	Route::group(['prefix' => 'general'], function () {
        Route::get('/printerFrontName', 'API\GeneralController@getPrinterFrontName');
        Route::get('/printerDimension', 'API\GeneralController@getPrinterDimension');
        Route::get('/jam-operasi/{cabang_id}', 'API\GeneralController@getOperatingHours');
        Route::get('/vendor-report', 'API\GeneralController@vendorReport');
    });

    Route::group(['prefix' => 'promo'], function () {
        Route::get('/', 'API\PromosiController@daftar');
    });
});

// ----- API V2 ----- //
Route::group(['prefix' => 'api/v2'], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'API\v2\ApiAuthController@login');
        Route::post('register', 'API\v2\ApiAuthController@register');
        Route::post('change_password', 'API\v2\ApiAuthController@change_password');
        Route::post('loginGoogle', 'API\v2\ApiAuthController@loginGoogle');
        Route::post('mobile_number', 'API\v2\ApiAuthController@updateMobileNumber');
    });

    Route::group(['prefix' => 'vehicle'], function () {
        Route::get('brands', 'API\v2\ApiVehicleController@brands');
        Route::get('type', 'API\v2\ApiVehicleController@type');
        Route::get('listVehicleByUser', 'API\v2\ApiVehicleController@listVehicleByUser');
        Route::get('singleVehicleByUser', 'API\v2\ApiVehicleController@singleVehicleByUser');
        Route::get('primaryVehicleByUser', 'API\v2\ApiVehicleController@primaryVehicleByUser');
        Route::post('saveVehicleByUser', 'API\v2\ApiVehicleController@saveVehicleByUser');
        Route::get('deleteVehicleByUser', 'API\v2\ApiVehicleController@deleteVehicleByUser');        
    });
    
    Route::group(['prefix' => 'service'], function () {
        Route::get('list', 'API\v2\ApiServicesController@list');
        Route::get('single', 'API\v2\ApiServicesController@single');
        Route::get('price', 'API\v2\ApiServicesController@price');
    });

    Route::group(['prefix' => 'booking'], function () {
        Route::get('branch', 'API\v2\ApiBookingController@branch');
        Route::get('branchTime', 'API\v2\ApiBookingController@branchTime');
        Route::get('list', 'API\v2\ApiBookingController@list');
        Route::get('detailList', 'API\v2\ApiBookingController@detailList');
        Route::post('save', 'API\v2\ApiBookingController@save');
    });

    Route::group(['prefix' => 'general'], function () {
        Route::get('categoryProduct', 'API\v2\ApiGeneralController@categoryProduct');
    });

    Route::group(['prefix' => 'slider'], function () {
        Route::get('list', 'API\v2\ApiSliderController@list');
    });

    Route::group(['prefix' => 'news'], function () {
        Route::get('list', 'API\v2\ApiNewsPromoController@list');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('list', 'API\v2\ApiProductController@list');
        Route::get('listByCategory', 'API\v2\ApiProductController@listByCategory');
    });

    Route::group(['prefix' => 'alamat'], function(){
        Route::get('/', 'API\v2\AlamatController@alamatList');
        Route::get('/{id}', 'API\v2\AlamatController@get');
        Route::get('/hapus/{id}', 'API\v2\AlamatController@hapus');
        Route::post('/tambah', 'API\v2\AlamatController@insert');
        Route::post('/edit', 'API\v2\AlamatController@update');
    });

    Route::group(['prefix' => 'homeservice'], function(){
        Route::post('/tambah', 'API\v2\HomeServiceController@insert');
    });

    Route::group(['prefix' => 'select'], function(){
        Route::get('/prov', 'API\v2\SelectController@provinsi');
        Route::get('/kota', 'API\v2\SelectController@kota');
        Route::get('/kec', 'API\v2\SelectController@kecamatan');
    });

});
