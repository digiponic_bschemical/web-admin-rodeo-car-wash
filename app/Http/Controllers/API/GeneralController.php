<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use CRUDBooster;

class GeneralController extends Controller
{
    public $successStatus = 200;

    public function getPrinterFrontName()
    {
        $printer = CRUDBooster::getSetting('printer_front');
        return $printer;
    }

    public function getPrinterDimension()
    {
        $dimension = CRUDBooster::getSetting('ukuran_kertas');
        return $dimension;
    }

    public function getOperatingHours($cabangId)
    {
        $data = DB::table('tb_cabang')
            ->select('id', 'jam_buka', 'jam_tutup', 'interval_jasa')
            ->where('id', $cabangId)
            ->first();

        return response()->json(['error' => false, 'msg' => 'Jam Operasional Cabang', 'data' => $data], $this->successStatus);
    }

    public function vendorReport()
    {
        $produk = "SELECT
    tb_jasa.id_jenis_jasa, tb_jasa.keterangan, 
    COUNT(tb_penjualan_jasa_detail.id_jasa) AS `count`
FROM
    tb_jasa
LEFT JOIN
    tb_penjualan_jasa_detail
    ON (tb_penjualan_jasa_detail.id_jasa=tb_jasa.id)
GROUP BY
    tb_jasa.id
ORDER BY
    tb_jasa.id";

        $vendor = "SELECT
    tb_jasa.id_jenis_jasa, 
    COUNT(tb_penjualan_jasa_detail.id_jasa) AS `count`
FROM
    tb_jasa
LEFT JOIN
    tb_penjualan_jasa_detail
    ON (tb_penjualan_jasa_detail.id_jasa=tb_jasa.id)
GROUP BY
    tb_jasa.id_jenis_jasa
ORDER BY
    tb_jasa.id_jenis_jasa";

        return response()->json(['error' => false, 'msg' => 'Jam Operasional Cabang', 'data' => $data], $this->successStatus);
    }
}
