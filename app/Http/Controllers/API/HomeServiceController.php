<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;

class HomeServiceController extends Controller{

    /* Input data pemesanan home service dengan asumsi inputan user adalah : 
        - cabang : nama cabang
        - tanggal_masuk : tanggal pemesanan masuk
        - nama_pelanggan : nama pelanggan yang memesan
        - merk_kendaraan : merk kendaraan (Honda, Toyota, Daihatsu, etc.)
        - nama_kendaraan : nama kendaraan (Brio, Agya, Ayla, Sirion, etc.)
        - nama_jasa : nama jasa yang digunakan
        - harga_jasa : harga jasa yang digunakan
        - kupon (opsional) : kupon dari promosi jika ada
         */
    public $successStatus = 200;

    public function insert(Request $request){
        $validator = Validator::make($request->all(), [
            'id_cabang' => 'required',
            'id_alamat' => 'required',
            'tanggal_masuk' => 'required',
            'email' => 'required',
            'merk_kendaraan' => 'required',
            'nama_kendaraan' => 'required',
            'no_polisi' => 'required',
            'services' => 'required'
        ], [
            'required'       => ':attribute harus diisi.',
            'unique'         => ':attribute harus unique.'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()], 401);
        }
        $input = $request->all();
        $id = DB::table('tb_penjualan_jasa')->where(DB::raw('DATE(tanggal)'), explode(' ', $input['tanggal_masuk'])[0])->max('id') + 1;
        $id_penjualan_jasa = DB::table('tb_penjualan_jasa')->max('id') + 1;
        $kode = 'HS' . date('dmy') . str_pad($id, 5, '0', STR_PAD_LEFT);
        $services = $input['services'];

        $totalBayar = 0;

        for($i = 0; $i < count($services); $i++){
            $id_jasa = DB::table('tb_jasa')->select('id')->where('keterangan', $nama_jasa[$i])->first()->id;
            DB::table('tb_penjualan_jasa_detail')->insert([
                'id' => DB::table('tb_penjualan_jasa_detail')->max('id') + 1,
                'id_penjualan_jasa' => $id_penjualan_jasa,
                'kode_penjualan_jasa' => $kode,
                'id_jasa' => $services[$i]['id_jasa'],
                'nama_jasa' => $services[$i]['nama_jasa'],
                'harga' => $services[$i]['harga'],
                'durasi' => $services[$i]['durasi'],
                'subtotal' => $services[$i]['subtotal'],
                'total' => $services[$i]['total']
            ]);
            $totalBayar += $services[$i]['total'];
        }

        DB::table('tb_penjualan_jasa')->insert([
            'id' => $id_penjualan_jasa,
            'id_cabang' => $input['id_cabang'],
            'id_alamat' => $input['id_alamat'],
            'kode' => $kode,
            'tanggal' => $input['tanggal_masuk'],
            'tanggal_masuk' => $input['tanggal_masuk'],
            'id_pelanggan' => DB::table('tb_pelanggan')->select('id')->where('email', $input['email'])->first()->id,
            'nama_pelanggan' => DB::table('tb_pelanggan')->select('nama')->where('email', $input['email'])->first()->nama,
            'id_merek_kendaraan' => DB::table('tb_merek_kendaraan')->select('id')->where('keterangan', $input['merk_kendaraan'])->first()->id,
            'merek_kendaraan' => $input['merk_kendaraan'],
            'id_kendaraan' => DB::table('tb_kendaraan')->select('id')->where('keterangan', $input['nama_kendaraan'])->first()->id,
            'nama_kendaraan' => $input['nama_kendaraan'],
            'nomor_polisi' => $input['no_polisi'],
            'subtotal' => $totalBayar,
            'total' => $totalBayar,
            'status_penjualan' => 51,
            'status_pembayaran' => 25,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 'api'
        ]);

        return response()->json(['error' => false, 'msg' => 'Data Berhasil Ditambahkan', 'data' => null], $this->successStatus);
    }
}