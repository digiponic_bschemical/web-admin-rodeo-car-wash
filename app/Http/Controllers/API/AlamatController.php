<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;

class AlamatController extends Controller{
    public $successStatus = 200;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function alamatList(Request $request){
        $input = $request->all();
        $email = $input['email'];
        $data = DB::table('tb_pelanggan_alamat')
            ->join('tb_pelanggan', 'tb_pelanggan_alamat.id_pelanggan', '=', 'tb_pelanggan.id')
            ->join('tb_provinsi', 'tb_pelanggan_alamat.id_provinsi', '=', 'tb_provinsi.id')
            ->join('tb_kota', 'tb_pelanggan_alamat.id_kota', '=', 'tb_kota.id')
            ->join('tb_kecamatan', 'tb_pelanggan_alamat.id_kecamatan', '=', 'tb_kecamatan.id')
            ->select('tb_pelanggan_alamat.id', 'tb_pelanggan_alamat.kode', 'tb_pelanggan_alamat.keterangan', 
            DB::raw('tb_provinsi.keterangan AS provinsi, tb_kota.keterangan AS kota, tb_kecamatan.keterangan AS kecamatan'), 'tb_pelanggan_alamat.alamat_lengkap')
            ->where('tb_pelanggan.email', $email)->get();
        return response()->json(['error' => false, 'msg' => 'Daftar Alamat Pelanggan', 'data' => $data], $this->successStatus);
    }

    public function get($id){
        $data = DB::table('tb_pelanggan_alamat')
            ->select('id', 'id_pelanggan', 'kode', 'keterangan', 'id_provinsi', 'id_kota', 'id_kecamatan', 'alamat_lengkap')
            ->where('id', $id)
            ->get();
        return response()->json(['error' => false, 'msg' => 'Daftar Alamat Pelanggan', 'data' => $data], $this->successStatus);
    }

    public function insert(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'keterangan' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'alamat_lengkap' => 'required'
        ], [
            'required'       => ':attribute harus diisi.',
            'unique'         => ':attribute harus unique.'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()], 401);
        }
        $input = $request->all();

        DB::table('tb_pelanggan_alamat')->insert([
            'id' => DB::table('tb_pelanggan_alamat')->max('id') + 1,
            'id_pelanggan' => DB::table('tb_pelanggan')->select('id')->where('email', $input['email'])->first()->id,
            'kode' => 'ALMT/' . str_pad(DB::table('tb_pelanggan_alamat')->max('id') + 1, 4, '0', STR_PAD_LEFT),
            'keterangan' => $input['keterangan'],
            'id_provinsi' => DB::table('tb_provinsi')->select('id')->where('keterangan', $input['provinsi'])->first()->id,
            'id_kota' => DB::table('tb_kota')->select('id')->where('keterangan', $input['kota'])->first()->id,
            'id_kecamatan' => DB::table('tb_kecamatan')->select('id')->where('keterangan', $input['kecamatan'])->first()->id,
            'alamat_lengkap' => $input['alamat_lengkap'],
            'created_at' => date('Y:m:d H:i:s'),
            'created_by' => 'api'
        ]);
        return response()->json(['error' => false, 'msg' => 'Data Berhasil Ditambahkan', 'data' => null], $this->successStatus);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'keterangan' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'alamat_lengkap' => 'required'
        ], [
            'required'       => ':attribute harus diisi.',
            'unique'         => ':attribute harus unique.'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()], 401);
        }        

        $input = $request->all();

        $data = [
            'keterangan' => $input['keterangan'],
            'id_provinsi' => DB::table('tb_provinsi')->select('id')->where('keterangan', $input['provinsi'])->first()->id,
            'id_kota' => DB::table('tb_kota')->select('id')->where('keterangan', $input['kota'])->first()->id,
            'id_kecamatan' => DB::table('tb_kecamatan')->select('id')->where('keterangan', $input['kecamatan'])->first()->id,
            'alamat_lengkap' => $input['alamat_lengkap'],
            'updated_at' => date('Y:m:d H:i:s'),
            'updated_by' => 'api'
        ];

        DB::table('tb_pelanggan_alamat')
            ->where('id', $input['id_alamat'])
            ->update($data);

        return response()->json(['error' => false, 'msg' => 'Data Berhasil Diubah', 'data' => null], $this->successStatus);
    }

    public function hapus($id){
        DB::table('tb_pelanggan_alamat')->where('id', $id)->delete();
        return response()->json(['error' => false, 'msg' => 'Alamat Pelanggan Dihapus', 'data' => null], $this->successStatus);
    }
}
