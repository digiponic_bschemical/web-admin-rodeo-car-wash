<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ApiProductController extends Controller
{
    public $table = 'tb_produk';
    public $response = array(
        'error' => false,
        'msg'   => null,
        'data'  => null
    );

    public function list(Request $request)
    {
        /* {
            keyword: string
        } */

        $data = DB::table($this->table)
                    ->select('id','kode','keterangan','gambar','harga','created_at')
                    ->where('id_jenis', 5)
                    ->where('status', 1)
                    ->whereNull('deleted_at');                    

        $param = $request->all();
        if(empty($param)){
            $list = $data->get();
            foreach ($list as $value) {
                if($value->gambar == null)
                    $value->gambar = url('/').'/logo.png';
                else
                    $value->gambar = url('/').'/'.$value->gambar;
            }

            $this->response['msg'] = 'List product';
            $this->response['data'] = $list;
    
            if(!$list){
                $this->response['error'] = true;
                $this->response['msg'] = 'Error get list product';
            }
        }else{
            $list = $data->where('keterangan','LIKE','%'.$request->keyword.'%')->get();
            $this->response['msg'] = 'List product by keterangan';
            $this->response['data'] = $list;

            foreach ($list as $value) {
                if($value->gambar == null)
                    $value->gambar = url('/').'/logo.png';
                else
                    $value->gambar = url('/').'/'.$value->gambar;
            }

            if(!$list){
                $this->response['error'] = true;
                $this->response['msg'] = 'Product not found';
            }
        }
        
        return response()->json($this->response, 200);        
    }

    public function listByCategory(Request $request)
    {
        /* {
            id: string
        } */

        $list = DB::table($this->table)
                    ->select('id','kode','keterangan','gambar','harga','created_at')
                    ->where('id_jenis', 5)
                    ->where('status', 1)
                    ->where('id_merek', $request->id)
                    ->whereNull('deleted_at')             
                    ->get();

        if(!$list){
            $this->response['error'] = true;
            $this->response['msg'] = 'Failed get data';
            return response()->json($this->response, 401);        
        }

        if(empty($list)){
            $this->response['msg'] = 'Product not found';
            $this->response['data'] = null;
        }else{
            
            foreach ($list as $value) {
                if($value->gambar == null)
                    $value->gambar = url('/').'/logo.png';
                else
                    $value->gambar = url('/').'/'.$value->gambar;
            }

            $this->response['msg'] = 'List product by keterangan';
            $this->response['data'] = $list;
        }        

        return response()->json($this->response, 200);         
    }
}
