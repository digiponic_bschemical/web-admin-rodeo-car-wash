<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;

class SelectController extends Controller{

    public $successStatus = 200;

    public function provinsi(){
      $data = DB::table('tb_provinsi')->select('id', 'kode', 'keterangan')->get();
      return response()->json(['error' => false, 'msg' => 'Daftar Provinsi', 'data' => $data], $this->successStatus);
    }

    public function kota(Request $request){
      $input = $request->all();
      $provinsi = $input['provinsi'];
      $id = DB::table('tb_provinsi')->select('id')->where('keterangan', $provinsi)->first()->id;
      $data = DB::table('tb_kota')
        ->select('id', 'kode', 'keterangan')->where('id_provinsi', $id)->get();
      return response()->json(['error' => false, 'msg' => 'Daftar Kota di Provinsi ' . $provinsi, 'data' => $data], $this->successStatus);
    }

    public function kecamatan(Request $request){
      $input = $request->all();
      $kota = $input['kota'];
      $id = DB::table('tb_kota')->select('id')->where('keterangan', $kota)->first()->id;
      $data = DB::table('tb_kecamatan')->select('id', 'keterangan')->where('id_kota', $id)->get();
      return response()->json(['error' => false, 'msg' => 'Daftar Kecamatan di ' . $kota, 'data' => $data], $this->successStatus);
    }
}