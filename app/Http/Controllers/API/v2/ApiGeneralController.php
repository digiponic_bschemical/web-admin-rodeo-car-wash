<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ApiGeneralController extends Controller
{
    public $response = array(
        'error' => false,
        'msg'   => null,
        'data'  => null
    );

    public function categoryProduct()
    {
        $list = DB::table('tb_general')->select('id','kode','keterangan')->where('id_tipe', 6)->whereNull('deleted_at')->get();
        $this->response['msg'] = 'List product categories';
        $this->response['data'] = $list;

        if(!$list){
            $this->response['error'] = true;
            $this->response['msg'] = 'Error get list product categories';
        }
        
        return response()->json($this->response, 200);
    }
}
