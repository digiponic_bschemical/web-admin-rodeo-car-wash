<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use DB;

class ApiBookingController extends Controller
{
    public $response = array(
        'error' => false,
        'msg'   => null,
        'data'  => null
    );

    public function branch()
    {
        $cabang = DB::table('tb_cabang as c')
                    ->join('tb_provinsi as p','p.id','=','c.id_provinsi')
                    ->join('tb_kota as k','k.id','=','c.id_kota')
                    ->join('tb_kecamatan as kc','kc.id','=','c.id_kecamatan')
                    ->select('c.id','c.kode_cabang','c.nama_cabang','c.telepon','c.email','c.alamat','p.keterangan as provinsi','k.keterangan as kota','kc.keterangan as kecamatan')
                    ->whereNull('c.deleted_at')
                    ->get();

        if($cabang){
            $this->response['msg'] = 'List all cabang';
            $this->response['data'] = $cabang;
        }else{
            $this->response['error'] = true;
            $this->response['msg'] = 'Error get list all cabang';
        }

        return response()->json($this->response, 200);
        
    }

    public function branchTime(Request $request)
    {
        /*if ($tgl < date("Y-m-d")){
            return response()->json(['error' => false, 'msg' => 'Daftar Waktu Booking', 'data' => null], $this->successStatus);
        }*/

        // get time configuration from setting
        $getTime = DB::table('tb_cabang')
            ->where('id', $request->id_branch)
            ->first();

        // get booked time
        $getServiceTime = DB::table('tb_penjualan_jasa')
            ->where('id_cabang', $request->id_branch)
            ->where('tanggal', '>', $request->date . " 02:00:00")
            ->where('tanggal', '<', $request->date . " 22:00:00")
            ->select('tanggal', 'total_slot')
            ->get();

        $slots = [];
        $time = $getTime->jam_buka;
        $time2 = $getTime->interval_jasa;
        $secs = strtotime($time2) - strtotime("00:00");
        $close = $getTime->jam_tutup;
//        $break = ["12:00", "12:30", $close];
        $break = [$close];

        // add booked time to break
        foreach ($getServiceTime as $gs) {
            $temp = date("H:i", strtotime($gs->tanggal));
            $break[] = $temp;
            // if service time more than 1 slot then add finish time break
            if ($gs->total_slot > 1) {
                for ($i = 1; $i < $gs->total_slot; $i++) {
                    $temp = date("H:i", strtotime($temp) + $secs);
                    $break[] = $temp;
                }
            }
        }

        // populating slot
        if ($getTime->jam_buka > date("H:i") || $request->date > date("Y-m-d")) {
            $slots[] = $time;
        }
        while ($time < $close) {
            $time = date("H:i", strtotime($time) + $secs);
            if ($request->date > date("Y-m-d")) {
//                if (!in_array($time, $break)) {
                $slots[] = $time;
//                }
            } else {
//                if (!in_array($time, $break) && $time > date("H:i")) {
                if ($time > date("H:i")) {
                    $slots[] = $time;
                }
            }
        }

        $res = array_diff($slots, $break);
        $result = [];
        foreach ($res as $r) {
            $result[] = $r;
        }

        $this->response['msg'] = 'List booking time';
        $this->response['data'] = $result;
        return response()->json($this->response, 200);
        
    }

    public function list(Request $request)
    {
        /* {
            email: string
        } */

        $user = DB::table('tb_pelanggan')->where('email', $request->email)->first();
        $book = DB::table('tb_penjualan_jasa as pj')
                    ->join('tb_cabang as c', 'c.id','=','pj.id_cabang')
                    ->join('tb_kendaraan as k', 'k.id','=','pj.id_kendaraan')
                    ->join('tb_general as g', 'g.id','=','pj.status')                    
                    ->select('pj.id','pj.kode','pj.tanggal','pj.tanggal_masuk','g.keterangan as status','k.gambar','c.nama_cabang as cabang')
                    ->where('pj.id_pelanggan', $user->id)
                    ->get();
        
        foreach ($book as $value) {
            $value->time_book = date('H:i', strtotime($value->tanggal)).' WIB';
            $value->time_check = (empty($value->tanggal_masuk)) ? '-' : date('H:i', strtotime($value->tanggal_masuk)).' WIB';
            if($value->gambar == null)
                $value->gambar = url('/').'/logo.png';
            else
                $value->gambar = url('/').'/'.$value->gambar;
        }

        if($book){
            $this->response['msg'] = 'List all booking services';
            $this->response['data'] = $book;
        }else{
            $this->response['error'] = true;
            $this->response['msg'] = 'Error get list all booking services';
            $this->response['data'] = $book;
        }

        return response()->json($this->response, 200);        
    }

    public function detailList(Request $request)
    {
        /* {
            id: int
        } */        

        $book_ = DB::table('tb_penjualan_jasa as pj')
                    ->join('tb_cabang as c', 'c.id','=','pj.id_cabang')
                    ->join('tb_kendaraan as k', 'k.id','=','pj.id_kendaraan')
                    ->join('tb_general as g', 'g.id','=','pj.status')                    
                    ->join('tb_general as gs', 'gs.id','=','pj.status_pembayaran')                    
                    ->select(
                        'pj.id','pj.kode','pj.tanggal','pj.tanggal_masuk','pj.nama_pelanggan',
                        'pj.merek_kendaraan','pj.nama_kendaraan','pj.nomor_polisi','pj.subtotal','pj.diskon_tipe',
                        'pj.diskon_nominal','pj.total','pj.status','g.keterangan as status_keterangan',
                        'pj.status_pembayaran','gs.keterangan as status_pembayaran_keterangan','pj.metode_pembayaran',
                        'c.nama_cabang as cabang','k.gambar')
                    ->where('pj.id', $request->id)
                    ->get();

        $book = $book_[0];
        
        if($book->metode_pembayaran == null)
            $book->metode_pembayaran_keterangan = '-';
        else
            $book->metode_pembayaran_keterangan = DB::table('tb_general')->where('id',$book->metode_pembayaran)->value('keterangan');

        if($book->gambar == null)
            $book->gambar = url('/').'/logo.png';
        else
            $book->gambar = url('/').'/'.$book->gambar;

        $detail = DB::table('tb_penjualan_jasa_detail as pjd')  
                    ->join('tb_jasa as j','j.id','=','pjd.id_jasa')
                    ->select('pjd.nama_jasa','pjd.harga','j.gambar')
                    ->where('pjd.id', $book->id)
                    ->get();
        
        foreach ($detail as $value) {
            if($value->gambar == null)
                $value->gambar = url('/').'/logo.png';
            else
                $value->gambar = url('/').'/'.$value->gambar;
        }
        $book->detail = $detail;

        if($book){
            $this->response = (array)$book;
            $this->response['error'] = false;
            $this->response['msg'] = 'Detail booking services';
        }else{
            $this->response['error'] = true;
            $this->response['msg'] = 'Error get detail booking services';
            $this->response['data'] = null;
        }

        return response()->json($this->response, 200);        
    }
    
    public function save(Request $request)
    {
        // return $request->all();
        /* {
            email: string,
            id_kendaraan: int,
            id_branch: int,
            nomor_polisi: string,
            date: string | YYYY-mm-dd | 2000-10-10,
            time: string | HH:ii | 10:10,
            services: json array 
        } */

        // selecting customer data
        $user = DB::table('tb_pelanggan')->where('email', $request->email)->first();
        
        // selecting customer vehicle
        $vehicle = DB::table('tb_pelanggan_kendaraan as k')
            ->join('tb_merek_kendaraan as mk', 'mk.id', '=', 'k.id_merek_kendaraan')
            ->join('tb_kendaraan as kd', 'kd.id', '=', 'k.id_kendaraan')
            ->select('kd.id_jenis_kendaraan', 'k.id_merek_kendaraan', 'mk.keterangan as merk', 'k.id_kendaraan', 'kd.keterangan as tipe', 'k.nomor_polisi')
            ->where('k.id_pelanggan', $user->id)
            ->where('k.id', $request->id_kendaraan)
            ->first();        

        // looping data foreach jasa
        $id_booking = DB::table('tb_penjualan_jasa')->max('id') + 1;
        $kode = 'BK' . date('dmy') . '' . str_pad($id_booking, 5, 0, STR_PAD_LEFT);

        $durasi = 0;
        $total = 0;                
        $services = $request->services;
        $len = count($services);

        for($i=0; $i<$len;$i++) {
            $services[$i]['id_penjualan_jasa'] = $id_booking;
            $services[$i]['kode_penjualan_jasa'] = $kode;
            $durasi += $services[$i]['durasi'];
            $total += $services[$i]['harga'];
        }        
        // hitung total slots
        $getTime = DB::table('tb_cabang')->where('id', $request->id_cabang)->first();
        $interval = date("i", strtotime($getTime->interval_jasa));

        DB::table('tb_penjualan_jasa')->insert(
            [
                'id'                 => $id_booking,
                'id_cabang'          => $request->id_cabang,
                'kode'               => $kode,
                'tanggal'            => $request->date . ' ' . $request->time,
                'id_pelanggan'       => $user->id,
                'nama_pelanggan'     => $user->nama,
                'id_merek_kendaraan' => $vehicle->id_merek_kendaraan,
                'merek_kendaraan'    => $vehicle->merk,
                'id_kendaraan'       => $vehicle->id_kendaraan,
                'nama_kendaraan'     => $vehicle->tipe,
                'nomor_polisi'       => $vehicle->nomor_polisi,
                'subtotal'           => $total,
                'total'              => $total,
                'total_slot'         => ceil($durasi / $interval),
                'status_penjualan'   => 28,
                'status_pembayaran'  => 25,
                'status'             => 47,
                'created_at'         => date("Y-m-d H:i:s"),
                'created_by'         => 'Booking System'
            ]
        );

        DB::table('tb_penjualan_jasa_detail')->insert($services);

        $this->response['kode'] = $kode; 
        $this->response['msg'] = 'Booking success';
        return response()->json($this->response, 200);
    }


}
