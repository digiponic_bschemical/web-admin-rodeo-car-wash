<?php

namespace App\Http\Controllers\API\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;

class ApiAuthController extends Controller
{
    public $table = 'tb_pelanggan';
    public $response = array(
        'error' => true,
        'msg'   => null,
        'data'  => null
    );

    public function login(Request $request)
    {
        /*
            {
                username: string,
                password: string
            }
        */

        $user = DB::table($this->table)
                        ->select('kode','nama','email','password','telepon','created_at')
                        ->where('email', $request->username)
                        ->OrWhere('telepon', $request->username)
                        ->first();                    

        if(empty($user)){
            $this->response['msg'] = 'Login failed, user not found';       
        }else{
            if(empty($user->password)){
                $this->response['msg'] = 'This email was registered by google auth';
            }else{
                if(Hash::check($request->password, $user->password)){
                    unset($user->password);
                    $this->response = $user;
                    $this->response->error = false;
                    $this->response->msg = 'Login success';
                }else{
                    $this->response['msg'] = 'Login failed, password not match';
                }            
            }
        }

        return response()->json($this->response, 200);
    }

    public function loginGoogle(Request $request)
    {
        /*
            {
                name: string,
                email: string  
            }
        */
        $check = DB::table($this->table)->select('kode','nama','email','telepon','created_at')->where('email', $request->email)->first();
        if(empty($check)){
            $kode = DB::table($this->table)->max('id') + 1;
            $kode = 'PLGN'.str_pad($kode,5,0,STR_PAD_LEFT);

            $data = array(
                'kode' => $kode,
                'nama' =>$request->name,
                'email' =>$request->email,
                'created_at'=>date('Y-m-d H:i:s')
            );
            $register = DB::table('tb_pelanggan')->insertGetId($data);

            if(!empty($register)){
                $this->response['error'] = false;
                $this->response['msg'] = 'Login success, new member';
                $this->response['firstLogin'] = true;
            }else{
                $this->response['error'] = true;
                $this->response['msg'] = 'Failed insert to database';                
            }
        }else{
            $this->response = $check;
            $this->response->error = false;
            $this->response->msg = 'Login success, old member';
            $this->response->firstLogin = (empty($check->telepon)) ? true : false;
            $this->response->sosmedLogin = true;
        }
        
        return response()->json($this->response, 200);        
    }

    public function updateMobileNumber(Request $request)
    {
        /* {
            email: string,
            mobile_number: string
        } */
        
        $check = DB::table($this->table)->where('email',$request->email)->where('telepon', $request->mobile_number)->exists();
        if($check){
            $this->response['error'] = true;
            $this->response['msg'] = 'Mobile number was registered, please use another';
        }else{            
            $update = DB::table($this->table)->where('email',$request->email)->update(['telepon' => $request->mobile_number]);            
            if($update){
                $this->response['error'] = false;
                $this->response['msg'] = 'Update mobile number success';
            }else{            
                $this->response['msg'] = 'Update mobile number failed';
            }
        }

        return response()->json($this->response, 200);
    }

    public function register(Request $request)
    {
        /*
            {
                nama: string,
                telepon: string,
                email: string,
                password: string,                
                confirm_password: string                
            }
        */

        $email = DB::table($this->table)->where('email', $request->email)->first();
        if(!empty($email)){
            $this->response['msg'] = 'Please use another email.';            
        }else{
            $telepon = DB::table($this->table)->where('telepon', $request->telepon)->first();
            if (!empty($telepon)) {
                $this->response['msg'] = 'Please use another mobile number';
            }else{
                $new = $request->all();
                $new['kode'] = DB::table($this->table)->max('id') + 1;
                $new['kode'] = 'PLGN'.str_pad($new['kode'],5,0,STR_PAD_LEFT);
                $new['password'] = Hash::make($new['password']);
                $new['created_at'] = date('Y-m-d H:i:s');
                $reg = DB::table($this->table)->insertGetId($new);
                if($reg){
                    $data = DB::table($this->table)->select('kode','nama','email','telepon','created_at')->where('id',$reg)->first();
                    $this->response = $data;
                    $this->response->error = false;
                    $this->response->msg = 'Register success, you can login now';
                }else{
                    $this->response['error'] = false;
                    $this->response['msg'] = 'Register failed, try again';
                    $this->response['data'] = null;
                }                
            }                        
        }   
        return response()->json($this->response, 200);     
    }

    public function change_password(Request $request)
    {
        /*
            {
                "email": string,
                "password": string
            }
        */

        $user = DB::table($this->table)->where('email', $request->email)->first();
        if(!Hash::check($request->password, $user->password)){
            $change = DB::table($this->table)->where('email', $request->email)->update(['password' => Hash::make($request->password)]);
            if($change){
                $this->response['error'] = false;
                $this->response['msg'] = 'Password successfully changed';
            }else{                
                $this->response['msg'] = 'Password failed to change';
            }
        }else{            
            $this->response['msg'] = 'Password cannot be the same as the old one';
        }

        return response()->json($this->response, 200);     
    }
}
