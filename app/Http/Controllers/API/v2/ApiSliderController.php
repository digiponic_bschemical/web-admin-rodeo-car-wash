<?php

namespace App\Http\Controllers\API\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Illuminate\Support\Facades\DB;

class ApiSliderController extends Controller
{

    public $table = 'tb_slider';
    public $response = array(
        'error' => false,
        'msg'   => null,
        'data'  => null
    );

    public function list()
    {
        $list = DB::table($this->table)->select('id','kode','keterangan','gambar')->whereNull('deleted_at')->get();

        foreach ($list as $value) {
            if($value->gambar == null)
                $value->gambar = url('/').'/logo.png';
            else
                $value->gambar = url('/').'/'.$value->gambar;
        }

        $this->response['msg'] = 'List slider';
        $this->response['data'] = $list;

        if(!$list){
            $this->response['error'] = true;
            $this->response['msg'] = 'Error get list slider';
        }
        
        return response()->json($this->response, 200);
        
    }
}
