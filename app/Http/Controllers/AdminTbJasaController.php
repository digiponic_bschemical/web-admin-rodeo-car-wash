<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminTbJasaController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id_jenis_jasa,asc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "tb_jasa";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];			
			$this->col[] = ["label"=>"Jenis","name"=>"id_jenis_jasa",'join'=>'tb_general,keterangan'];
			$this->col[] = ["label"=>"Gambar","name"=>"gambar","image"=>true];
			$this->col[] = ["label"=>"Keterangan","name"=>"keterangan"];
			$this->col[] = ["label"=>"Harga","name"=>"harga",'callback_php'=>'number_format($row->harga,0,",",".")'];
			// $this->col[] = ["label"=>"Vendor","name"=>"id_vendor","join"=>"tb_vendor,nama"];
			# END COLUMNS DO NOT REMOVE THIS LINE
			
			$kode = DB::table('tb_jasa')->max('id') + 1;
			$kode = 'JS'.str_pad($kode,5,0,STR_PAD_LEFT);

			# START FORM DO NOT REMOVE THIS LINE
      $this->form = [];
      $this->form[] = ['label'=>'','name'=>'kode','type'=>'hidden','width'=>'col-sm-10','value'=>$kode];
			$this->form[] = ['label'=>'Jenis Jasa','name'=>'id_jenis_jasa','type'=>'radio','validation'=>'required','width'=>'col-sm-10','datatable'=>'tb_general,keterangan','datatable_where'=>'id_tipe = 4','inline'=>'1'];
			$this->form[] = ['label'=>'Vendor','name'=>'id_vendor','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'tb_vendor,nama'];
			$this->form[] = ['label'=>'Keterangan','name'=>'keterangan','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10','placeholder'=>'Cth: Quick Wash Plus Waxing'];
      // $this->form[] = ['label'=>'Insentif','name'=>'insentif','type'=>'money','width'=>'col-sm-10','help'=>'Nominal insentif yang akan dibagikan ke kru / teknisi','placeholder'=>'Cth: 8.000'];
      $this->form[] = ['label'=>'Harga','name'=>'harga','type'=>'money','width'=>'col-sm-10','help'=>'Harga jasa','placeholder'=>'Cth: 8.000'];
      $this->form[] = ['label'=>'Durasi','name'=>'durasi','type'=>'number','width'=>'col-sm-10','help'=>'Biaya pengerjaan jasa (dalam menit)','placeholder'=>'Cth: 30'];
			$this->form[] = ['label'=>'Gambar','name'=>'gambar','type'=>'upload','validation'=>'image|max:1000','width'=>'col-sm-10','help'=>'Tipe file yang didukung: JPG, JPEG, PNG | MAX 1MB', 'encrypt' => true];
			$this->form[] = ['label'=>'Deskripsi','name'=>'deskripsi','type'=>'textarea','validation'=>'required|string|min:50|max:5000','width'=>'col-sm-10','placeholder'=>'Minimal deskripsi 50 kata'];
			// $this->form[] = ['label'=>'Penggunaan Bahan','name'=>'bahan_jasa','type'=>'child','width'=>'col-sm-10','table'=>'tb_jasa_bahan','foreign_key'=>'id_jasa'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];

			//// $this->form[] = ['label'=>'Jenis Jasa','name'=>'id_jenis_jasa','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'tb_general,keterangan','datatable_where'=>'id_tipe = 4'];
			//$this->form[] = ['label'=>'Jenis Jasa','name'=>'id_jenis_jasa','type'=>'radio','validation'=>'required','width'=>'col-sm-10','datatable'=>'tb_general,keterangan','datatable_where'=>'id_tipe = 4','inline'=>true,'value'=>6];
			//$this->form[] = ['label'=>'Vendor','name'=>'id_vendor','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'tb_vendor,nama','value'=>1];
			//$this->form[] = ['label'=>'Keterangan','name'=>'keterangan','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10','placeholder'=>'Cth: Quick Wash Plus Waxing'];
			//$this->form[] = ['label'=>'Insentif','name'=>'insentif','type'=>'money','width'=>'col-sm-10','help'=>'Nominal insentif yang akan dibagikan ke kru / teknisi','placeholder'=>'Cth: 8.000'];
			//$this->form[] = ['label'=>'Gambar','name'=>'gambar','type'=>'upload','validation'=>'image|max:1000','width'=>'col-sm-10','help'=>'Tipe file yang didukung: JPG, JPEG, PNG | MAX 1MB','encrypt'=>true];
			//$this->form[] = ['label'=>'Deskripsi','name'=>'deskripsi','type'=>'textarea','validation'=>'required|string|min:50|max:5000','width'=>'col-sm-10','placeholder'=>'Minimal deskripsi 50 kata'];
			//// $this->form[] = ['label'=>'Menggunakan Bahan','name'=>'is_komposisi','type'=>'radio','width'=>'col-sm-10','dataenum'=>'1|Ya;0|Tidak','value'=>0,'inline'=>true,'help'=>'Menu pengisian bahan akan muncul setelah data disimpan'];
			//$columns[] = ['label'=>'Bahan','name'=>'id_bahan_jasa','type'=>'select','datatable'=>'tb_bahan_jasa,keterangan'];
			//$columns[] = ['label'=>'Qty','name'=>'quantity','type'=>'number'];
			//$columns[] = ['label'=>'Satuan','name'=>'satuan','type'=>'text','readonly'=>true];
			//$this->form[] = ['label'=>'Penggunaan Bahan','name'=>'bahan_jasa','type'=>'child','columns'=>$columns,'table'=>'tb_jasa_bahan','foreign_key'=>'id_jasa'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();
			// $this->sub_module[] = ['title'=>'Bahan Baku','path'=>'tb_durasi_jasa','parent_columns'=>'kode,keterangan','foreign_key'=>'id_jasa','button_color'=>'warning','button_icon'=>'fa fa-bars','showIf'=>'[is_komposisi] == 1'];
			// $this->sub_module[] = ['title'=>'Durasi','path'=>'tb_durasi_jasa','parent_columns'=>'kode,keterangan','foreign_key'=>'id_jasa','button_color'=>'info','button_icon'=>'fa fa-clock-o'];
			// $this->sub_module[] = ['title'=>'Harga','path'=>'tb_harga_jasa','parent_columns'=>'kode,keterangan','foreign_key'=>'id_jasa','button_color'=>'danger','button_icon'=>'fa fa-money'];

	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
			$this->script_js = "
				$(function(){
					$('#penggunaanbahanid_bahan_jasa').change(function(){
						var id = $(this).val();
						$.ajax({
							method: 'GET',
							url: '".CRUDBooster::apipath('bahan/single')."',
							data: {id: id},
							success: function(res){
								console.log(res);								
								$('#penggunaanbahansatuan').val(res.satuan);
							},
							error: function(err){
								console.log(err);
							}
						});
					});
				});
			";


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here
			$postdata['created_by'] = CRUDBooster::myName();
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
			$postdata['updated_by'] = CRUDBooster::myName();
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here
			DB::table('tb_jasa')->where('id',$id)->update([
				'deleted_by'	=> CRUDBooster::myName()
			]);
	    }



	    //By the way, you can still create your own method in here... :) 


	}